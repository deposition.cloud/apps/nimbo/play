const { Configurer } = require('@deposition.cloud/conf')

module.exports = {
  conf: new Configurer({
    required: ['port']
  })
}
