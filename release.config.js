module.exports = {
  branches: [
    '+([0-9])?(.{+([0-9]),x}).x',
    'main',
    'next',
    'next-major', {
      name: 'beta',
      prerelease: true
    }, {
      name: 'alpha',
      prerelease: true
    }
  ],
  plugins: [
    '@semantic-release/commit-analyzer',
    '@semantic-release/release-notes-generator',
    '@semantic-release/gitlab',
    ['@semantic-release/npm', {
      npmPublish: false
    }],
    ['@semantic-release/changelog',
      {
        changelogFile: 'docs/CHANGELOG.md',
        changelogTitle: '# Play Changelog'
      }],
    ['@semantic-release/git',
      {
        assets: [
          'package.json',
          'docs/CHANGELOG.md'
        ],
        // eslint-disable-next-line no-template-curly-in-string
        message: 'chore(release): ${nextRelease.version} [skip ci]\n\n${nextRelease.notes}'
      }],
    ['@eclass/semantic-release-docker',
      {
        baseImageName: 'play',
        registries: [{
          url: 'registry.gitlab.com',
          imageName: 'registry.gitlab.com/deposition.cloud/apps/nimbo/play',
          user: 'CI_REGISTRY_USER',
          password: 'CI_REGISTRY_PASSWORD'
        }, {
          url: 'docker.io',
          imageName: 'docker.io/depositioncloud/nimbo-play',
          user: 'DOCKER_REGISTRY_USER',
          password: 'DOCKER_REGISTRY_PASSWORD'
        }],
        additionalTags: ['next', 'next-major', 'beta', 'alpha']
      }]
  ]
}
