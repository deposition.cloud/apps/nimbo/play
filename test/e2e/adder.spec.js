const axios = require('axios')

const url = 'http://localhost:3000'

describe('e2e', function () {
  it('adder', async function () {
    let res

    try {
      res = await axios({
        method: 'post',
        url: `${url}/graphql`,
        timeout: 4000,
        data: {
          query: `query play($input: AdderInputType) {
                    adder(input: $input) {
                      sum
                    }
                  }
                  `,
          variables: `{ "input":
                        {
                          "x": 10,
                          "y": 20
                        }
                      }`
        }
      })
    } catch (e) {
      console.error(e)
    }
    console.log(JSON.stringify(res.data.data.adder.sum))
    expect(res.data.data.adder.sum).to.equal(30)
  })
})
