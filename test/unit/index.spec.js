const { add } = require('../../src/add')

describe('all', function () {
  it('NODE_ENV set to test', function () {
    expect(process.env.NODE_ENV).to.equal('test')
  })

  it('add', function () {
    const a = 1
    const b = 2

    expect(add(a, b)).to.equal(a + b)
  })
})
