const chai = require('chai')
global.mockery = require('mockery')
global.sinon = require('sinon')

global.chai = chai
chai.use(require('sinon-chai'))
chai.use(require('chai-as-promised'))
chai.use(require('dirty-chai'))

global.path = require('path')
global.expect = chai.expect
