FROM node:lts-alpine AS base

WORKDIR /usr/src/app

COPY package*.json ./
COPY .npmrc ./


# deps
FROM base AS deps
COPY . .

# install and save production node_modules
RUN npm ci --only=production
RUN npm audit fix --only=prod
RUN mv node_modules /tmp/prod_node_modules

# install all node_modules
RUN npm ci
RUN npm audit fix


# test
FROM deps AS test
ENV NODE_ENV=test
RUN npm run lint && npm run test:unit


# release
FROM base AS release
# copy prviously saved production node_modules
COPY --from=deps /tmp/prod_node_modules ./node_modules
EXPOSE 80
CMD npm run start
