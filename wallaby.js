module.exports = function (wallaby) {
  return {
    files: [
      'index.js',
      'package.json',
      'lib/**/*.js',
      'test/**/*.js',
      '!test/**/*.spec.js'
    ],
    tests: [
      'test/unit/**/*spec.js',
      'test/e2e/**/*spec.js'
    ],
    setup: wallaby => {
      const mocha = wallaby.testFramework

      const chai = require('chai')
      const sinon = require('sinon')

      chai.use(require('sinon-chai'))

      // setup sinon hooks
      mocha.suite.beforeEach('sinon before', function () {
        if (this.sinon == null) {
          this.sinon = sinon.createSandbox()
        }
      })
      mocha.suite.afterEach('sinon after', function () {
        if (this.sinon && typeof this.sinon.restore === 'function') {
          this.sinon.restore()
        }
      })

      global.expect = require('chai').expect
    },
    env: {
      type: 'node',
      runner: 'node',
      params: {
        env: 'NODE_ENV=test'
      }
    },
    testFramework: 'mocha',
    debug: true,
    runAllTestsInAffectedTestFile: true
  }
}
