# Play on Cloud Nimbo

## Publish

To cut a beta release

``` bash
git checkout -b beta
# make code changes
git add -A :/
git commit -m 'fix(feature): make a fix'
git push -u origin beta
```

## GitLab-CI Local

Boot up a docker daemon accessible from WSL2 via TCP

``` bash
docker run --privileged --name docker-dind -d -e DOCKER_TLS_CERTDIR="" -p 23750:2375 docker:dind --insecure-registry registry.lan:32000
```

Might need to point docker to the docker daemon container

``` bash
DOCKER_HOST=tcp://localhost:23750 gitlab-ci-local --privileged <job-name>
```

Or via the convenience npm script

``` bash
npm run gitlab -- <job-name>
```

## Troubleshooting

Easy to forget docker commands:

``` bash
docker run -it docker-dind /bin/sh # get in and debug the daemon
docker stop docker-dind # stop running daemon
docker rm docker-dind # delete before rerunning with modified arguments
```

Ref: [GitLab CI Local](https://github.com/firecow/gitlab-ci-local)
