module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es2021: true
  },
  extends: [
    'plugin:mocha/recommended',
    'plugin:mocha-cleanup/recommended',
    'plugin:chai-expect/recommended',
    'plugin:chai-friendly/recommended',
    'standard'
  ],
  plugins: [
    'mocha',
    'mocha-cleanup',
    'chai-expect',
    'chai-friendly'
  ],
  parserOptions: {
    ecmaVersion: 12
  },
  globals: {
    expect: true
  }
}
