# Play Changelog

## [0.0.4-beta.1](https://gitlab.com/deposition.cloud/apps/nimbo/play/compare/v0.0.3...v0.0.4-beta.1) (2021-05-10)


### Bug Fixes

* **cd:** test beta channel ([f70104a](https://gitlab.com/deposition.cloud/apps/nimbo/play/commit/f70104ace9352d9e8647cbe2361fe473cf20ae8a))
* **docs:** correct acronym ([f4e59de](https://gitlab.com/deposition.cloud/apps/nimbo/play/commit/f4e59de86290e763a243198bc2e56c5094b903ae))

## [0.0.3](https://gitlab.com/deposition.cloud/apps/nimbo/play/compare/v0.0.2...v0.0.3) (2021-05-09)


### Bug Fixes

* **cd:** change changelog title to markdown ([7b37d4c](https://gitlab.com/deposition.cloud/apps/nimbo/play/commit/7b37d4c97525e4eec32bbab5d1e4b29b9c41d9d8))

Play Changelog

## [0.0.2](https://gitlab.com/deposition.cloud/apps/nimbo/play/compare/v0.0.1...v0.0.2) (2021-05-09)


### Bug Fixes

* **cd:** add changelog title ([38f874e](https://gitlab.com/deposition.cloud/apps/nimbo/play/commit/38f874ef13cbd89619e40e2e1dccf139ef63c6e6))
* **cd:** fix release step for semantic release docker ([65a6136](https://gitlab.com/deposition.cloud/apps/nimbo/play/commit/65a61364f19ffaad17c65d04b6a5ad0b5ac3dee6))

## [0.0.2](https://gitlab.com/deposition.cloud/apps/nimbo/play/compare/v0.0.1...v0.0.2) (2021-05-09)


### Bug Fixes

* **cd:** fix release step for semantic release docker ([65a6136](https://gitlab.com/deposition.cloud/apps/nimbo/play/commit/65a61364f19ffaad17c65d04b6a5ad0b5ac3dee6))
